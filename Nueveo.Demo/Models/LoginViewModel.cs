﻿using FluentValidation.Attributes;
using Nueveo.Demo.Validation;

namespace Nueveo.Demo.Models
{
    [Validator(typeof(LoginViewModelValidator))]
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
