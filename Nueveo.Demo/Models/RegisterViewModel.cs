﻿using FluentValidation.Attributes;
using Nueveo.Demo.Validation;

namespace Nueveo.Demo.Models
{
    [Validator(typeof(RegisterViewModelValidator))]
    public class RegisterViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }

        
        public bool EqualsPassword() => Password == RepeatPassword;
    }
}
