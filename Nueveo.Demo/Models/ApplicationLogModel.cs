﻿using System;
using Nuevo.Domain.Entity;

namespace Nueveo.Demo.Models
{
    public class ApplicationLogModel
    {
        public int LogId { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationUrl { get; set; }
        public string ApplicationDescription { get; set; }
        public DateTime ApplicationLogTime { get; set; }
        public ApplicationStatus ApplicationStatus { get; set; }
    }
}
