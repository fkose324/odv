﻿using System;
using Nuevo.Core.Infrastructure.Concrete.Mapper;

namespace Nueveo.Demo.Models
{
    public class ApplicationCreateModel : BaseDto
    {
        public int? ApplicationId { get; set; }
        public string AppName { get; set; }
        public string AppUri { get; set; }
        public int? DayInput { get; set; }
        public int? HourInput { get; set; }
        public int? MinuteInput { get; set; }
        public int? SecondInput { get; set; }

        public int Interval => (int)new TimeSpan(DayInput ?? 0, HourInput ?? 0, MinuteInput ?? 0, SecondInput ?? 0).TotalSeconds;
    }
}
