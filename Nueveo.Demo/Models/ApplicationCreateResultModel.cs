﻿using Nuevo.Core.Infrastructure.Concrete.Mapper;

namespace Nueveo.Demo.Models
{
    public class ApplicationCreateResultModel : BaseDto
    {
        public bool State { get; set; }
        public int ApplicationId { get; set; }
        public bool HasUpdate { get; set; }
    }
}
