﻿using FluentValidation;
using Nueveo.Demo.Models;

namespace Nueveo.Demo.Validation
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(x => x.Email).EmailAddress().NotEmpty();
            RuleFor(x => x.Password).NotEmpty().MinimumLength(6);
            RuleFor(x => x.RememberMe).Must(x => true);
        }
    }
}
