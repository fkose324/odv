﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Nueveo.Demo.Models;
using Nuevo.Core.Infrastructure.Concrete;
using Nuevo.Domain.Account;

namespace Nueveo.Demo.Validation
{
    public class RegisterViewModelValidator : AbstractValidator<RegisterViewModel>
    {
        public RegisterViewModelValidator()
        {
            RuleFor(x => x.Email).EmailAddress().NotEmpty();
            RuleFor(x => x.UserName).NotEmpty().MaximumLength(20);
            RuleFor(x => x.Password).NotEmpty().MinimumLength(6);

            RuleFor(x => x.UserName).Must(CheckUserName).WithMessage("User already exists");
            RuleFor(x => x.UserName).Must(EmailCheck).WithMessage("Email already exists");
        }

        private bool EmailCheck(string arg)
        {
            var userManager = EngineContext.Current.Resolve<UserManager<AppUser>>();

            return userManager.FindByEmailAsync(arg).GetAwaiter().GetResult() == null;
        }

        private bool CheckUserName(string arg)
        {
            var userManager = EngineContext.Current.Resolve<UserManager<AppUser>>();

            return userManager.FindByNameAsync(arg).GetAwaiter().GetResult() == null;
        }
    }
}
