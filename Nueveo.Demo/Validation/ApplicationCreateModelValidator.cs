﻿using FluentValidation;
using Nueveo.Demo.Models;

namespace Nueveo.Demo.Validation
{
    public class ApplicationCreateModelValidator : AbstractValidator<ApplicationCreateModel>
    {
        public ApplicationCreateModelValidator()
        {
            RuleFor(x => x.AppName).NotEmpty().Length(3);
            RuleFor(x => x.AppUri).NotNull();
            RuleFor(x => x.Interval).Must(HasInterval).WithMessage("Please enter valid interval.");
        }

        private bool HasInterval(int arg) => arg > 0;
        
    }
}
