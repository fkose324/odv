﻿(function () {
    $(document).on('click', '.edit-application', function (e) {
        var id = $(this).attr('data-id');
        $.get('/application/Load/' + id,
            function (result) {
                $('#dayInput').val(result.dayInput);
                $('#hourInput').val(result.hourInput);
                $('#minuteInput').val(result.minuteInput);
                $('#secondInput').val(result.secondInput);
                $('#appUri').val(result.appUri);
                $('#appName').val(result.appName);
                $('#applicationId').val(result.applicationId);
                $('#frmButton').html('UPDATE');
                $('#myModal').modal('show');
            }).fail(function (err) {
                alert('Unable to update application, please try again later');
            });;
    });

    $(document).on('click', '.remove-application', function (e) {
        if (confirm('Are you sure, you want to delete this application.')) {
            var id = $(this).attr('data-id');

            $.get('/application/Remove/' + id,
                function (result) {
                    $('#application_' + id).remove();
                }).fail(function (err) {
                    alert('Unable to delete application, please try again later');
                });
        }
    });


    // Application Form 
    var appForm = {
        appName: {
            presence: true,
            length: {
                minimum: 3
            }
        },
        appUri: {
            presence: true,
            format:
                'https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,}'
        }
    }


    $('#appCreateForm').on("submit",
        function (ev) {
            ev.preventDefault();
            handleFormSubmit(this);
        });


    function handleFormSubmit(form) {
        // First we gather the values from the form
        var values = validate.collectFormValues(form);
        // then we validate them against the constraints
        var errors = validate(values, appForm);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        // And if all constraints pass we let the user know
        if (!errors) {
            if (!hasInputValue($('#dayInput')) &&
                !hasInputValue($('#hourInput')) &&
                !hasInputValue($('#minuteInput')) &&
                !hasInputValue($('#secondInput'))) {
                var result = confirm('Your control period settings, automatically set to 1 hour?');

                if (result) {
                    $('#hourInput').val(1);
                    showSuccess();
                }
            } else {
                showSuccess();
            }
        }
    }

    function hasInputValue(input) {
        if (input.val() === "" || input.val() === 0)
            return false;

        return true;
    }

    // Updates the inputs with the validation errors
    function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function (input) {
            // Since the errors can be null if no errors were found we need to handle
            // that
            showErrorsForInput(input, errors && errors[input.name]);
        });
    }

    // Shows the errors for a specific input
    function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "input-group");
        // Find where the error messages will be insert into
        if (formGroup === null)
            return;

        var messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
            // we first mark the group has having errors
            formGroup.classList.add("has-error");
            // then we append all the errors
            _.each(errors, function (error) {
                addError(messages, error);
            });
        } else {
            // otherwise we simply mark it as success
            formGroup.classList.add("has-success");
        }
    }

    // Recusively finds the closest parent that has the specified class
    function closestParent(child, className) {
        if (!child || child == document) {
            return null;
        }
        if (child.classList.contains(className)) {
            return child;
        } else {
            return closestParent(child.parentNode, className);
        }
    }

    function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function (el) {
            el.parentNode.removeChild(el);
        });
    }

    // Adds the specified error with the following markup
    // <p class="help-block error">[message]</p>
    function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.classList.add("text-danger");
        block.innerHTML = error;

        messages.appendChild(block);
    }

    function showSuccess() {
        // We made it \:D/
        var data = $($('#appCreateForm')).serialize();

        $.ajax({
            type: "POST",
            url: "/application/create",
            data: data,
            success: function (result) {
                if (result.state) {
                    var dom = formatAndGetDom(result);

                    if (result.hasUpdate) {
                        $('#application_' + result.applicationId).remove();
                    }

                    $('#applicationStateParent').append(dom);
                    $('#myModal').modal('hide');
                    clearInputs();

                } else {
                    alert("Failed to create application, try again later");
                }
            },
            error: function (error) {
                alert("Failed to create application, try again later");
            }
        });
    }


    var applicationTemplate =
        '<div class="col-md-4" id="application_{applicationId}">' +
        '<div class="card">' +
        '<button class="btn btn-primary btn-link text-white float-right edit-application  pl-0 pr-0" style="position: absolute; top: -30px; right: 25px"' +
        'data-id="{applicationId}"><i class="material-icons">edit</i></button>' +
        '<a href="/applicationlog/{applicationId}" class="btn btn-primary btn-link text-white float-right pl-0 pr-0" style="position: absolute; top: -30px; right: 55px"' +
        'data-id="@itr.ApplicationId">' +
        ' <i class="material-icons">search</i>' +
        '</a>' +
        '<button type="button" class="btn btn-primary btn-link text-white float-right remove-application pl-0 pr-0" style="position: absolute; top: -30px; right: 85px"' +
        ' data-id="{applicationId}">' +
        '  <i class="material-icons">delete</i>' +
        '   </button>' +
        '<div id="applicationHeader_{applicationId}" class="card-header card-header-info">' +
        '<h4 class="card-title" id="applicationName_{applicationId}">{applicationName}</h4 > ' +
        '<p class="category" id="applicationState_{applicationId}">Waiting for check</p> ' +
        '</div>' +
        '<div class="card-body">' +
        '<image src = "https://www.google.com/s2/favicons?domain={appUri}" /> ' +
        '{appUri}' +
        '</div> ' +
        '</div> ' +
        '</div>'


    function formatAndGetDom(result) {

        var dom = applicationTemplate
            .replace(/\{appUri}/g, $('#appUri').val())
            .replace(/\{applicationName}/g, $('#appName').val())
            .replace(/\{applicationId}/g, result.applicationId);

        return dom;
    }



})();

function clearInputs() {
    $('#dayInput').val('');
    $('#hourInput').val('');
    $('#minuteInput').val('');
    $('#secondInput').val('');
    $('#appUri').val('');
    $('#appName').val('');
    $('#applicationId').val('');
}

$("#myModal").on("hidden.bs.modal", function () {
    clearInputs();
    $('#frmButton').html('CREATE');

});


$(document).ready(function () {
    setInterval(function () {
        $.get('/application/CheckStatus', function (result) {
            result.forEach(function (el) {
                var elm = document.getElementById('applicationState_' + el.applicationId);
                SetColorState(el);
                $(elm).html(el.description);
            });
        }).fail({

        })
    }, 5000);


    function SetColorState(el) {
        var elm = document.getElementById('applicationHeader_' + el.applicationId);
        $(elm).removeClass("card-header-info").removeClass("card-header-success")
            .removeClass("card-header-danger");

        if (el.status >= 200 && el.status < 300)
            $(elm).addClass("card-header-success");
        else
            $(elm).addClass("card-header-danger");
    }
})
