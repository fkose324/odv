﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Nuevo.Core.Infrastructure.Concrete;
using Nuevo.Domain.Account;
using Nuevo.Domain.Entity;
using Nuevo.Service.App;
using Nuevo.Service.Infrastructure.Abstract;
using Nuevo.Service.Notification;
using Nuevo.Web.Framework.BackgroundWorker;

namespace Nueveo.Demo.Cron
{
    public class SchedulerCron : BackgroundService
    {
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var applicationService = EngineContext.Current.Resolve<IApplicationService>();
            var applicationRecordService = EngineContext.Current.Resolve<IGenericService<ApplicationRecord>>();
            var logger = EngineContext.Current.Resolve<ILogger<SchedulerCron>>();
            var userManager = EngineContext.Current.Resolve<UserManager<AppUser>>();
            var emailNotificationService = EngineContext.Current.Resolve<IEmailService>();

            object srw_lock = new object();
            while (!stoppingToken.IsCancellationRequested)
            {
                var unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds - 1000;

                lock (srw_lock)
                {
                    try
                    {
                        foreach (var app in applicationService.GetAll(x =>
                            !x.IsDeleted && x.NextCheckTime < unixTimestamp).GetAwaiter().GetResult())
                        {
                            try
                            {
                                var uri = app.Url.Contains("http") ? app.Url : $"http://{app.Url}";

                                var result = new HttpClient().GetAsync(new Uri(uri), stoppingToken).GetAwaiter()
                                    .GetResult();

                                if (result != null)
                                {

                                    var statusMsg = ((ApplicationStatus)(int)result.StatusCode).ToString();
                                    applicationRecordService.Add(new ApplicationRecord()
                                    {
                                        Application = app,
                                        Description = statusMsg,
                                        ApplicationId = app.ApplicationId,
                                        Status = (ApplicationStatus)(int)result.StatusCode
                                    }).GetAwaiter().GetResult();


                                    if (unixTimestamp >= app.NextEmailTime && (int)result.StatusCode >= 300)
                                    {
                                        var msg =$"Application {statusMsg}-{result.RequestMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()}";
                                        emailNotificationService.SendNotify($"{app.Name} application error",msg, app.UserName).GetAwaiter().GetResult();

                                        app.NextEmailTime = unixTimestamp + TimeSpan.FromMinutes(30).TotalSeconds;
                                    }

                                }
                            }
                            catch(HttpRequestException ex)
                            {

                                if (unixTimestamp >= app.NextEmailTime)
                                {

                                    emailNotificationService.SendNotify($"{app.Name} application error",
                                        $"Cannot get application response <br> { ex.Message}", app.UserName).GetAwaiter().GetResult();

                                    app.NextEmailTime = unixTimestamp + TimeSpan.FromMinutes(30).TotalSeconds;
                                }

                                applicationRecordService.Add(new ApplicationRecord()
                                {
                                    Application = app,
                                    Description = "No Response",
                                    ApplicationId = app.ApplicationId,
                                    Status = 0
                                }).GetAwaiter().GetResult();
                            }


                            app.NextCheckTime = unixTimestamp + (app.Interval);
                            app.ModifiedDate = DateTime.Now;
                            applicationService.Update(app).GetAwaiter().GetResult();
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Critical, ex.Message);
                    }
                }

                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
