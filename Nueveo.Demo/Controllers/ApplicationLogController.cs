﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nueveo.Demo.Models;
using Nuevo.Domain.Entity;
using Nuevo.Service.Infrastructure.Abstract;
using Nuevo.Web.Framework.Controller;

namespace Nueveo.Demo.Controllers
{
    [Authorize]
    public class ApplicationLogController : BaseController
    {
        private readonly IGenericService<ApplicationRecord> _applicationRecordService;

        public ApplicationLogController(IGenericService<ApplicationRecord> applicationRecordService)
        {
            _applicationRecordService = applicationRecordService;
        }

        [HttpGet("applicationlog/{id?}")]
        public async Task<IActionResult> Index(int? id)
        {
            var query = _applicationRecordService.SetQuery().Include(x => x.Application)
                .Where(x => !x.IsDeleted && x.Application.UserName == User.Identity.Name);

            if (id.HasValue && id.Value > 0)
                query = query.Where(x => x.ApplicationId == id.Value).OrderByDescending(x => x.ApplicationRecordId)
                    .Take(50);
            else
               query= query.OrderByDescending(x => x.ApplicationRecordId).Take(50);


            var model = (await _applicationRecordService.GetAll(query, false)).Select(x => new ApplicationLogModel
            {
                ApplicationDescription = x.Description,
                ApplicationLogTime = x.CreateDate,
                ApplicationName = x.Application.Name,
                ApplicationStatus = x.Status,
                ApplicationUrl = x.Application.Url,
                LogId = x.ApplicationRecordId
            });
            return await ViewAsync(model);
        }
    }
}