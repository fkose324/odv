﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Nueveo.Demo.Models;
using Nuevo.Domain.Account;
using Nuevo.Web.Framework.Controller;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Nueveo.Demo.Controllers
{
    public class AccountController : BaseController
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;

        public AccountController(SignInManager<AppUser> signInManager, UserManager<AppUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> Create()
        {
            return await ViewAsync();
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel model, string returnUrl = "/")
        {
            if (ModelState.IsValid)
            {
                if (model.EqualsPassword())
                {
                    var user = new AppUser()
                    {
                        UserName = model.UserName,
                        Email = model.Email
                    };

                    var result = await _userManager.CreateAsync(user);

                    if (result == IdentityResult.Success)
                    {
                        result = await _userManager.AddPasswordAsync(user, model.Password);

                        if (result == IdentityResult.Success)
                            return Redirect(returnUrl);
                        
                        ModelState.AddModelError("Create","Failed to create account, try again later."); 
                        await _userManager.DeleteAsync(user);
                    }
                    else
                        ModelState.AddModelError("Create","Failed to create account, try again later.");
                }
                else
                {
                    ModelState.AddModelError("Pass", "Passwords dont match.");
                }
            }
            return await ViewAsync(model);
        }

        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            return await ViewAsync();
        }


        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = "/")
        {
            ModelState.Remove("RememberMe");
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var result =
                        await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false);

                    if (result == SignInResult.Success)
                    {
                        return Redirect(returnUrl);
                    }
                }

                ModelState.AddModelError("Wrong", "Login failed, Username or password incorrect.");
            }
            return await ViewAsync(model);
        }


        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/");
        }
    }
}