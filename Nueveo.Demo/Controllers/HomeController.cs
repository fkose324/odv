﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nuevo.Service.Notification;

namespace Nueveo.Demo.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController(IEmailService emailService)
        {
        }
        public IActionResult Index()
        {
            return View();
        }

    }
}
