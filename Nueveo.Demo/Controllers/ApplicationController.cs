﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nueveo.Demo.Models;
using Nuevo.Domain.Entity;
using Nuevo.Service.App;
using Nuevo.Service.Infrastructure.Abstract;

namespace Nueveo.Demo.Controllers
{
    [Authorize]
    public class ApplicationController : Controller
    {
        private readonly IApplicationService _appService;
        private readonly IGenericService<ApplicationRecord> _appRecordService;

        public ApplicationController(IApplicationService appService, IGenericService<ApplicationRecord> appRecordService)
        {
            _appService = appService;
            _appRecordService = appRecordService;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<object> Create(ApplicationCreateModel model)
        {
            var resultModel = new ApplicationCreateResultModel() { ApplicationId = -1, State = false, HasUpdate = false};
            if (ModelState.IsValid)
            {
                Application entity = null;
                if (model.ApplicationId > 0)
                {
                    entity = await _appService.GetById(model.ApplicationId);

                    if (entity != null && entity.UserName == User.Identity.Name)
                    {
                        entity.Name = model.AppName;
                        entity.Interval = model.Interval;
                        entity.Url = model.AppUri;
                        entity.ModifiedDate = DateTime.Now;

                        entity = await _appService.Update(entity);

                        if (entity != null && entity.ApplicationId > 0)
                        {
                            resultModel.ApplicationId = entity.ApplicationId;
                            resultModel.State = true;
                            resultModel.HasUpdate = true;
                        }
                    }
                }
                else
                {
                    entity = await _appService.Add(new Application()
                    {
                        Url = model.AppUri,
                        Interval = model.Interval,
                        Name = model.AppName,
                        NextCheckTime = 0,
                        UserName = User.Identity.Name,
                        NextEmailTime =  0
                    });

                    if (entity != null && entity.ApplicationId > 0)
                    {
                        resultModel.ApplicationId = entity.ApplicationId;
                        resultModel.State = true;
                    }
                }
            }

            return Json(resultModel);
        }


        public async Task<object> Load(int? id)
        {
            var result = new ApplicationCreateModel();

            if (id.HasValue)
            {
                var application = await _appService.GetById(id);

                if (application != null && application.UserName == User.Identity.Name)
                {
                    result.ApplicationId = application.ApplicationId;
                    result.AppName = application.Name;
                    result.AppUri = application.Url;

                    var timeSpan = TimeSpan.FromSeconds(application.Interval);
                    result.DayInput = timeSpan.Days;
                    result.HourInput = timeSpan.Hours;
                    result.MinuteInput = timeSpan.Minutes;
                    result.SecondInput = timeSpan.Seconds;


                    return Json(result);
                }
            }

            return NotFound();
        }

        public async Task<IActionResult> Remove(int? id)
        {
            if (id.HasValue)
            {
                var entity = await _appService.GetById(id);

                if (entity != null && entity.UserName == User.Identity.Name)
                {
                    _ = await _appService.Remove(entity);
                    return Ok();
                }
            }

            return NotFound();
        }


        public async Task<object> CheckStatus()
        {
            var query = _appService.SetQuery().Include(x => x.ApplicationRecords).Where(x => !x.IsDeleted && x.UserName == User.Identity.Name).Select(x => x.ApplicationRecords.Where(y => !y.IsDeleted)
                .OrderByDescending(y => y.ApplicationRecordId).FirstOrDefault());

            var entities = await _appRecordService.GetAll(query, false);

            return Json(entities);
        }

    }
}