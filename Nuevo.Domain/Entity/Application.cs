﻿using System.Collections.Generic;
using Nuevo.Domain.Infrastucture;

namespace Nuevo.Domain.Entity
{
    public class Application : BaseEntity
    {
        public Application()
        {
            this.ApplicationRecords = new HashSet<ApplicationRecord>();
        }

        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Interval { get; set; }
        public double NextCheckTime { get; set; }

        public string UserName { get; set; }
        public double NextEmailTime { get; set; }


        public IEnumerable<ApplicationRecord> ApplicationRecords { get; set; }
    }
}
