﻿using Microsoft.EntityFrameworkCore;
using Nuevo.Domain.Entity;
using Nuevo.Domain.Infrastucture;

namespace Nuevo.Domain.Mapping
{
    public class ApplicationRecordMap : IEntityRegister
    {
        public void Register(ModelBuilder builder)
        {
            builder.Entity<ApplicationRecord>(e =>
            {
                e.HasKey(x => x.ApplicationRecordId);
                e.Property(x => x.Description).HasMaxLength(100);
            });
        }
    }
}
