﻿using Microsoft.EntityFrameworkCore;
using Nuevo.Domain.Entity;
using Nuevo.Domain.Infrastucture;

namespace Nuevo.Domain.Mapping
{
    public class ApplicationMap : IEntityRegister
    {
        public void Register(ModelBuilder builder)
        {
            builder.Entity<Application>(e =>
            {
                e.HasKey(x => x.ApplicationId);
                e.Property(x => x.Name).HasMaxLength(80);
                e.Property(x => x.Url).HasMaxLength(1200);
            });
        }
    }
}
