﻿using System;
namespace Nuevo.Domain.Infrastucture
{

    public class BaseEntity
    {
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public BaseEntity()
        {
            CreateDate = ModifiedDate = DateTime.Now;
        }
    }
}
