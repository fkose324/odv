﻿using Microsoft.EntityFrameworkCore;

namespace Nuevo.Domain.Infrastucture
{
    public interface IEntityRegister
    {
        void Register(ModelBuilder builder);
    }
}
