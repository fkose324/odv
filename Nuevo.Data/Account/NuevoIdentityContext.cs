﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Nuevo.Domain.Account;

namespace Nuevo.Data.Account
{
    public class NuevoIdentityContext : IdentityDbContext<AppUser, AppRole, int>
    {
        public NuevoIdentityContext(DbContextOptions<NuevoIdentityContext> options) : base(options)
        {

        }
    }
}
