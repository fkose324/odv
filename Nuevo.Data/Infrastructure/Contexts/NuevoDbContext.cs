﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Core.Infrastructure.Concrete;
using Nuevo.Domain.Infrastucture;

namespace Nuevo.Data.Infrastructure.Contexts
{
    public class NuevoDbContext : DbContext
    {

        #region Ctors
        public NuevoDbContext()
        {

        }

        public NuevoDbContext(DbContextOptions<NuevoDbContext> options) : base(options)
        {

        }
        #endregion

        #region Overriding

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var entityRegistrars = EngineContext.Current
                .Resolve<ITypeFinder>().FindClassesOfType<IEntityRegister>()
                .ToList();

            var instances = entityRegistrars
                .Select(mapperConfiguration => (IEntityRegister)Activator.CreateInstance(mapperConfiguration))
                .ToList();

            foreach (var instance in instances)
            {
                instance.Register(modelBuilder);
            }

            base.OnModelCreating(modelBuilder);
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // bla bla bla
            }

            base.OnConfiguring(optionsBuilder);
        }

        #endregion
    }
}
