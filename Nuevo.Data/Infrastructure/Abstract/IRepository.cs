﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Nuevo.Data.Infrastructure.Abstract
{
    public interface IRepository<TEntity, TContext> : IDisposable
    {
        Task<TEntity> GetById(object Id);
        Task<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties);
        Task<IList<TEntity>> GetAll(IQueryable<TEntity> query = null, bool? asNoTracking = true);
        Task<IList<TEntity>> GetAll(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties);
        Task<TEntity> Add(TEntity entity);
        Task<IEnumerable<TEntity>> Add(IEnumerable<TEntity> entity);
        Task<TEntity> Remove(TEntity entity);
        Task<IEnumerable<TEntity>> Remove(IEnumerable<TEntity> entities);
        Task<bool> Remove(Expression<Func<TEntity, bool>> filter);
        Task<TEntity> Update(TEntity entity);
        Task<IEnumerable<TEntity>> Update(IEnumerable<TEntity> entities);
        Task<int> Count(Expression<Func<TEntity, bool>> filter = null);
        Task<bool> Any(Expression<Func<TEntity, bool>> filter = null);
        IQueryable<TEntity> SetQuery();
    }
}
