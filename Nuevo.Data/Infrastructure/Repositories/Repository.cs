﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nuevo.Data.Infrastructure.Abstract;
using Nuevo.Domain.Infrastucture;

namespace Nuevo.Data.Infrastructure.Repositories
{
    public class Repository<TEntity, TContext> : IRepository<TEntity, TContext> where TEntity : BaseEntity
                                                                      where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly IConfiguration _configuration;
        private readonly ILogger<Repository<TEntity, TContext>> _logger;

        public Repository(TContext context, IConfiguration configuration, ILogger<Repository<TEntity, TContext>> logger)
        {
            _context = context;
            _configuration = configuration;
            _logger = logger;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public IQueryable<TEntity> SetQuery()
        {
            return _context.Set<TEntity>() as IQueryable<TEntity>;
        }

        public async Task<TEntity> GetById(object id)
        {
            try
            {
                return await _context.FindAsync<TEntity>(id);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
                return null;
            }
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true,
            params Expression<Func<TEntity, object>>[] properties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>() as IQueryable<TEntity>;

            if (asNoTracking == true)
                query = query.AsNoTracking();

            try
            {
                if (properties.Length <= 0)
                {
                    return await query.FirstOrDefaultAsync(filter);
                }
                else
                {
                    query = properties.Aggregate(query, (current, property) => current.Include(property));
                    return await query.FirstOrDefaultAsync(filter);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<IList<TEntity>> GetAll(IQueryable<TEntity> query = null, bool? asNoTracking = true)
        {
            try
            {
                if (query == null)
                    query = SetQuery();

                if (asNoTracking == true)
                    query = query.AsNoTracking();

                return await query.ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<IList<TEntity>> GetAll(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>() as IQueryable<TEntity>;

            try
            {
                if (properties != null)
                    query = properties.Aggregate(query, (current, property) => current.Include(property));

                if (asNoTracking == true)
                    query = query.AsNoTracking();

                var result = filter == null ? await query.ToListAsync() : await query.Where(filter).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            try
            {
                entity.IsDeleted = false;
                entity.CreateDate = DateTime.Now;
                entity.ModifiedDate = DateTime.Now;

                await _context.Set<TEntity>().AddAsync(entity);
                await _context.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<TEntity>> Add(IEnumerable<TEntity> entities)
        {
            try
            {
                var baseEntities = entities as TEntity[] ?? entities.ToArray();
                await _context.Set<TEntity>().AddRangeAsync(baseEntities);
                await _context.SaveChangesAsync();

             

                return baseEntities;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<TEntity> Remove(TEntity entity)
        {
            try
            {
                entity.IsDeleted = true;
                await Update(entity);

                return entity;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<IEnumerable<TEntity>> Remove(IEnumerable<TEntity> entities)
        {
            try
            {
                var baseEntities = entities as TEntity[] ?? entities.ToArray();
                foreach (var entity in baseEntities)
                {
                    entity.IsDeleted = true;
                }

                await Update(baseEntities);


                return entities;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Update(entity);
                _context.Entry(entity).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<IEnumerable<TEntity>> Update(IEnumerable<TEntity> entities)
        {
            try
            {
                var baseEntities = entities as TEntity[] ?? entities.ToArray();
                _context.Set<TEntity>().UpdateRange(baseEntities);
                _context.Entry(baseEntities).State = EntityState.Modified;

                await _context.SaveChangesAsync();
                
                return entities;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return null;
            }
        }

        public async Task<int> Count(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            try
            {
                return await query.AsNoTracking().CountAsync(filter);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);

                return 0;
            }
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            try
            {
                return await query.AsNoTracking().AnyAsync(filter);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }

        public async Task<bool> Remove(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            try
            {
                var results = await query.Where(filter).ToListAsync();

                await Remove(results);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, ex.Message);
                return false;
            }
        }
    }
}
