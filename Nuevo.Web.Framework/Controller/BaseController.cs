﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Nuevo.Web.Framework.Controller
{
    public class BaseController :Microsoft.AspNetCore.Mvc.Controller
    {
        public async Task<ViewResult> ViewAsync()
        {
            return await Task.Run(View);
        }

        public async Task<ViewResult> ViewAsync(object model)
        {
            return await Task.Run(() => View(model));
        }

        public async Task<ViewResult> ViewAsync(string viewName)
        {
            return await Task.Run(() => View(viewName));
        }

        public async Task<ViewResult> ViewAsync(string viewName, object model)
        {
            return await Task.Run(() => View(viewName, model));
        }
    }
}
