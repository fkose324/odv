﻿using Microsoft.AspNetCore.Builder;
using Nuevo.Core.Infrastructure.Concrete;

namespace Nuevo.Web.Framework.Middleware
{
    public static class ApplicationBuilderExtensions
    {
        public static void ConfigureRequestPipeline(this IApplicationBuilder application)
        {
            EngineContext.Current.ConfigureRequestPipeline(application);
        }
    }
}
