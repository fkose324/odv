﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Data.Account;
using Nuevo.Data.Infrastructure.Contexts;

namespace Nuevo.Web.Framework.Middleware.Startups
{
    public class DatabaseStartup : INuevoStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<NuevoDbContext>(op => op.UseSqlServer(configuration.GetConnectionString("NuevoDb"), b => b.MigrationsAssembly("Nueveo.Demo")));
            services.AddDbContext<NuevoIdentityContext>(o => o.UseSqlServer(configuration.GetConnectionString("AccountDb"), b => b.MigrationsAssembly("Nueveo.Demo")));
        }

        public void Configure(IApplicationBuilder application)
        {
        }

        public int Priority => 2;
    }
}
