﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nuevo.Core.Infrastructure.Abstract;

namespace Nuevo.Web.Framework.Middleware.Startups
{
    public class RouterStartup : INuevoStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {

        }

        public void Configure(IApplicationBuilder application)
        {
            application.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public int Priority => 6;
    }
}
