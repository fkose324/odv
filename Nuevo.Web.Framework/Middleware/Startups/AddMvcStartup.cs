﻿using System;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Core.Infrastructure.Concrete;
using Nuevo.Data.Account;
using Nuevo.Domain.Account;
using Nuevo.Domain.Infrastucture;
using Nuevo.Web.Framework.FluentValidation;

namespace Nuevo.Web.Framework.Middleware.Startups
{
    public class AddMvcStartup : INuevoStartup
    {
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            IWebHostEnvironment env = EngineContext.Current.Resolve<IWebHostEnvironment>();
            services.Configure<EmailSettings>(configuration.GetSection("EmailSettings"));


            services.AddControllersWithViews();
            if (env.IsDevelopment())
            {
                services.AddControllersWithViews().AddRazorRuntimeCompilation();
            }

            var mvcBuilder = services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            });


            mvcBuilder.SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            ValidatorOptions.LanguageManager.Enabled = false;

            mvcBuilder.AddFluentValidation(c =>
            {
                c.ValidatorFactoryType = typeof(NuevoValidatorFactory);
                c.ImplicitlyValidateChildProperties = true;
            });


            services.AddIdentity<AppUser, AppRole>(options =>
                {
                    options.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                    options.User.RequireUniqueEmail = true;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 0;
                    options.Lockout.MaxFailedAccessAttempts = 3;
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                    options.Lockout.DefaultLockoutTimeSpan = new TimeSpan(0, 30, 0);
                    options.Lockout.AllowedForNewUsers = true;

                }).AddEntityFrameworkStores<NuevoIdentityContext>()
                .AddDefaultTokenProviders()
                .AddSignInManager<SignInManager<AppUser>>();


            services.ConfigureApplicationCookie(opt =>
            {
                opt.Cookie.Name = "Nuevo.AuthCookie";
                opt.Cookie.SameSite = env.IsProduction() ? SameSiteMode.Strict : SameSiteMode.None;
                opt.Cookie.HttpOnly = true;
                opt.LoginPath = "/account/login";
                opt.AccessDeniedPath = "/account/create";
                opt.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                opt.LogoutPath = "/account/logout";
            });


            services.AddAuthentication();
        }

        public void Configure(IApplicationBuilder application)
        {
            IConfiguration configuration = EngineContext.Current.Resolve<IConfiguration>();
            IWebHostEnvironment env = EngineContext.Current.Resolve<IWebHostEnvironment>();

            application.UseAuthentication();

            if (env.IsDevelopment())
            {
                application.UseDeveloperExceptionPage();
            }

            bool.TryParse(configuration?["CookieSecure"], out bool cookieSecure);

            application.UseCookiePolicy(new CookiePolicyOptions
            {
                Secure = cookieSecure
                    ? CookieSecurePolicy.Always
                    : (env.IsProduction() ? CookieSecurePolicy.SameAsRequest : CookieSecurePolicy.None)
            });

            application.UseRouting();
            application.UseStaticFiles();
            application.UseAuthentication();
            application.UseAuthorization();
        }

        public int Priority => 4;
    }
}
