﻿using System.Linq;
using Autofac;
using Microsoft.Extensions.Configuration;
using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Core.Infrastructure.Concrete;
using Nuevo.Data.Infrastructure.Abstract;
using Nuevo.Data.Infrastructure.Repositories;
using Nuevo.Service.Infrastructure.Abstract;
using Nuevo.Service.Infrastructure.Concrete;

namespace Nuevo.Web.Framework.Middleware.Startups
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            var Configuration = EngineContext.Current.Resolve<IConfiguration>();

            builder.RegisterGeneric(typeof(Repository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(GenericService<>)).As(typeof(IGenericService<>)).InstancePerLifetimeScope();
            var scopedRegistrars = typeFinder.FindClassesOfType<IScopedRegister>(false);

            foreach (var scopedRegistrar in scopedRegistrars.Where(x => x.IsClass))
            {
                builder.RegisterType(scopedRegistrar)
                    .As(scopedRegistrar.GetInterfaces().FirstOrDefault(x => x.Name == $"I{scopedRegistrar.Name}")).InstancePerLifetimeScope();
            }

            var transientRegistrars = typeFinder.FindClassesOfType<ITransientRegister>(false);

            foreach (var transientRegistrar in transientRegistrars.Where(x => x.IsClass))
            {
                builder.RegisterType(transientRegistrar)
                    .As(transientRegistrar.GetInterfaces().FirstOrDefault(x => x.Name == $"I{transientRegistrar.Name}")).InstancePerDependency();
            }

            var singletonRegistrars = typeFinder.FindClassesOfType<ISingletonRegister>(false);

            foreach (var singletonRegistrar in singletonRegistrars.Where(x => x.IsClass))
            {
                builder.RegisterType(singletonRegistrar)
                    .As(singletonRegistrar.GetInterfaces().FirstOrDefault(x => x.Name == $"I{singletonRegistrar.Name}")).SingleInstance();
            }
        }

        public int Priority => 1;
    }
}
