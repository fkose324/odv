﻿namespace Nuevo.Core.Extensions
{
    public static class UrlExtension
    {
        public static string ClearHttpTag(this string url, bool wwwRm = false)
        {
            if (wwwRm)
                url = url.Replace("www.", "");

            return url.Replace("http://", "").Replace("https://", "");
        }
    }
}
