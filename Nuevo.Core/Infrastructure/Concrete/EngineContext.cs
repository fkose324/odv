﻿using System.Runtime.CompilerServices;
using Nuevo.Core.Infrastructure.Abstract;

namespace Nuevo.Core.Infrastructure.Concrete
{
    public class EngineContext
    {
        #region Methods

        /// <summary>
        /// Create a static instance of the Nuevo engine.
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static INuevoEngine Create()
        {
            //create NuevoEngine as engine
            return Singleton<INuevoEngine>.Instance ?? (Singleton<INuevoEngine>.Instance = new NuevoEngine());
        }

        /// <summary>
        /// Sets the static engine instance to the supplied engine. Use this method to supply your own engine implementation.
        /// </summary>
        /// <param name="engine">The engine to use.</param>
        /// <remarks>Only use this method if you know what you're doing.</remarks>
        public static void Replace(INuevoEngine engine)
        {
            Singleton<INuevoEngine>.Instance = engine;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the singleton Nuevo engine used to access Nuevo services.
        /// </summary>
        public static INuevoEngine Current
        {
            get
            {
                if (Singleton<INuevoEngine>.Instance == null)
                {
                    Create();
                }

                return Singleton<INuevoEngine>.Instance;
            }
        }

        #endregion
    }
}
