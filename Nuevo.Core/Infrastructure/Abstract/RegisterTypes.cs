﻿
namespace Nuevo.Core.Infrastructure.Abstract
{
    public interface IScopedRegister
    {
    }

    public interface ITransientRegister
    {

    }

    public interface ISingletonRegister
    {

    }
}
