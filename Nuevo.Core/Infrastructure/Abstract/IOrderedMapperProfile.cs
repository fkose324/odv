﻿namespace Nuevo.Core.Infrastructure.Abstract
{
    public interface IOrderedMapperProfile
    {
        int Order { get; }

    }
}
