﻿using Autofac;

namespace Nuevo.Core.Infrastructure.Abstract
{
    public interface IDependencyRegistrar
    {
        void Register(ContainerBuilder builder, ITypeFinder typeFinder);
        int Priority { get; }
    }
}
