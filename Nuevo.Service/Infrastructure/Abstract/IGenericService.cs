﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Nuevo.Service.Infrastructure.Abstract
{
    public interface IGenericService<TEntity>
    {
        Task<TEntity> GetById(object id);
        Task<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties);
        Task<IList<TEntity>> GetAll(IQueryable<TEntity> query = null, bool? asNoTracking = true);
        Task<IList<TEntity>> GetAll(Expression<Func<TEntity, bool>> filter = null, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties);
        Task<TEntity> Add(TEntity entity);
        Task<IEnumerable<TEntity>> Add(IEnumerable<TEntity> entity);
        Task<TEntity> Remove(TEntity entity);
        Task<IEnumerable<TEntity>> Remove(IEnumerable<TEntity> entities);
        Task<TEntity> Update(TEntity entity);
        Task<IEnumerable<TEntity>> Update(IEnumerable<TEntity> entities);
        Task<int> Count(Expression<Func<TEntity, bool>> filter);
        Task<bool> Any(Expression<Func<TEntity, bool>> filter);
        IQueryable<TEntity> SetQuery();
    }
}
