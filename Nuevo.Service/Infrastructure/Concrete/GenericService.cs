﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Nuevo.Data.Infrastructure.Abstract;
using Nuevo.Data.Infrastructure.Contexts;
using Nuevo.Service.Infrastructure.Abstract;

namespace Nuevo.Service.Infrastructure.Concrete
{
    public class GenericService<TEntity> : IGenericService<TEntity>
    {
        private readonly IRepository<TEntity, NuevoDbContext> _genericDal;

        public GenericService(IRepository<TEntity, NuevoDbContext> genericDal)
        {
            _genericDal = genericDal;
        }

        public virtual IQueryable<TEntity> SetQuery()
        {
            return _genericDal.SetQuery();
        }

        public virtual async Task<TEntity> Add(TEntity entity)
        {
            return await _genericDal.Add(entity);
        }

        public virtual async Task<IEnumerable<TEntity>> Add(IEnumerable<TEntity> entity)
        {
            return await _genericDal.Add(entity);
        }

        public virtual async Task<bool> Any(Expression<Func<TEntity, bool>> filter)
        {
            return await _genericDal.Any(filter);
        }

        public virtual async Task<int> Count(Expression<Func<TEntity, bool>> filter)
        {
            return await _genericDal.Count(filter);
        }

        public virtual async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties)
        {
            return await _genericDal.Get(filter, asNoTracking, properties);
        }

        public virtual async Task<IList<TEntity>> GetAll(IQueryable<TEntity> query = null, bool? asNoTracking = true)
        {
            return await _genericDal.GetAll(query, asNoTracking);
        }

        public virtual async Task<IList<TEntity>> GetAll(Expression<Func<TEntity, bool>> filter, bool? asNoTracking = true, params Expression<Func<TEntity, object>>[] properties)
        {
            return await _genericDal.GetAll(filter, asNoTracking, properties);
        }

        public virtual async Task<TEntity> GetById(object id)
        {
            return await _genericDal.GetById(id);
        }

        public virtual async Task<TEntity> Remove(TEntity entity)
        {
            return await _genericDal.Remove(entity);
        }

        public virtual async Task<IEnumerable<TEntity>> Remove(IEnumerable<TEntity> entities)
        {
            return await _genericDal.Remove(entities);
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            return await _genericDal.Update(entity);
        }

        public virtual async Task<IEnumerable<TEntity>> Update(IEnumerable<TEntity> entities)
        {
            return await _genericDal.Update(entities);
        }
    }
}
