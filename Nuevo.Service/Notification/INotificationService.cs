﻿using System.Threading.Tasks;

namespace Nuevo.Service.Notification
{
    public interface INotificationService
    {
        Task SendNotify(string title, string description, string userName);
    }
}
