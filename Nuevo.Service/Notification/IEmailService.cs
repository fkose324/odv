﻿using Nuevo.Core.Infrastructure.Abstract;

namespace Nuevo.Service.Notification
{
    public interface IEmailService :INotificationService, ISingletonRegister
    {
    }
}
