﻿using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Data.Infrastructure.Abstract;
using Nuevo.Data.Infrastructure.Contexts;
using Nuevo.Domain.Entity;
using Nuevo.Service.Infrastructure.Concrete;

namespace Nuevo.Service.App
{
    public class ApplicationService : GenericService<Application>, IApplicationService, IScopedRegister
    {
        public ApplicationService(IRepository<Application, NuevoDbContext> repository) : base(repository)
        {
        }
    }
}
