﻿using Nuevo.Core.Infrastructure.Abstract;
using Nuevo.Domain.Entity;
using Nuevo.Service.Infrastructure.Abstract;

namespace Nuevo.Service.App
{
    public interface IApplicationService : IGenericService<Application>, IScopedRegister
    {

    }
}
